# @swis/fluo-stylelint-config-smacss

> The SMACSS stylelint config for Fluo projects.

Extends [`fluo-stylelint-config`](https://bitbucket.org/swisnl/fluo-stylelint-config/).

# Howto

Run: `npm install @swis/fluo-stylelint-config-smacss stylelint-order stylelint-scss`

In your `.stylelintrc`:
```
{
    "extends": "@swis/fluo-stylelint-config-smacss"
}
```
